package model.data_structures_test;

import model.data_structures.ListaEncadenada;
import junit.framework.TestCase;


public class ListaEncadenadaTest extends TestCase
{
	private ListaEncadenada<String> listaEncadenada;

	private void setUpEscenario1( )
	{
		listaEncadenada = new ListaEncadenada<String>();
	}

	private void setUpEscenario2( )
	{
		listaEncadenada = new ListaEncadenada<String>();

		/*0*/ listaEncadenada.agregarElementoFinall("Juan Pablo");
		/*1*/ listaEncadenada.agregarElementoFinall("Sebasti�n");
		/*2*/ listaEncadenada.agregarElementoFinall("Pedro");
		/*3*/ listaEncadenada.agregarElementoFinall("Camila");
		/*4*/ listaEncadenada.agregarElementoFinall("Sara");
	}

	public void testAgregarElementoFinal()
	{
		setUpEscenario1( );
		listaEncadenada.agregarElementoFinall("Mar�a");
		assertEquals("El elemento deberia ser Mar�a", "Mar�a", listaEncadenada.darElemento(0));
		
		setUpEscenario2( );
		listaEncadenada.agregarElementoFinall("Andrea");
		assertEquals("El elemento deberia ser Andrea", "Andrea", listaEncadenada.darElemento(5));
	}
	
	public void testDarElemento()
	{
		setUpEscenario2();
		assertEquals("El elemento deberia ser Sara", "Sara", listaEncadenada.darElemento(4));
	}

	public void testDarNumeroDeElementos()
	{
		setUpEscenario1( );
		listaEncadenada.agregarElementoFinall("Estructura");
		listaEncadenada.darNumeroElementos();
		assertEquals(1 , listaEncadenada.darNumeroElementos() );

		setUpEscenario2( );
		listaEncadenada.darNumeroElementos();
		assertEquals(5 , listaEncadenada.darNumeroElementos());
	}
	
	public void testDarElementoPosicionActual()
	{
		setUpEscenario2();
		listaEncadenada.darElementoPosicionActual();
		assertEquals("Debe ser Juan Pablo", "Juan Pablo", listaEncadenada.darElementoPosicionActual());
				
		setUpEscenario2();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals("Deber�a ser Sara", "Sara", listaEncadenada.darElementoPosicionActual());

	}
	
	public void testAvanzarSiguientePosicion()
	{
		setUpEscenario1();
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals("El nodo no deberia existir", false , listaEncadenada.avanzarSiguientePosicion());
		
		setUpEscenario2();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();	
		assertEquals("El nodo no deberia existir", false , listaEncadenada.avanzarSiguientePosicion());
		
		setUpEscenario2();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals("El nodo deberia existir", true , listaEncadenada.avanzarSiguientePosicion());
	}
	
	public void testRetrocederPosicionAnterior()
	{
		setUpEscenario2();
		listaEncadenada.retrocederAnteriorPosicion();
		assertEquals("No se debe ejecutar porque hay un solo nodo", false , listaEncadenada.retrocederAnteriorPosicion());
		
		setUpEscenario2();
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals("Debe retroceder a la posici�n anterior", true , listaEncadenada.retrocederAnteriorPosicion());
	}
}
