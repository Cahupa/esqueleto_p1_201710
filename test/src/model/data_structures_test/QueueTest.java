package model.data_structures_test;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase
{
	private Queue<String> queue;

	private void setUpEscenario1( )
	{
		queue = new Queue<String>();
	}

	private void setUpEscenario2( )
	{
		queue = new Queue<String>();

		/*0*/ queue.enqueue("Dylan");
		/*1*/ queue.enqueue("Pablo");
		/*2*/ queue.enqueue("Juan Manuel");
		/*3*/ queue.enqueue("Melissa");
		/*4*/ queue.enqueue("laura");
		/*5*/ queue.enqueue("Angie");
	}

	public void testEnqueue()
	{
		setUpEscenario1( );
		queue.enqueue("Carlos");
		assertEquals("El elemento retornado deberia ser Carlos", "Carlos", queue.dequeue());

		setUpEscenario2( );
		queue.enqueue("Sara");
		for(int i = 0; i <= 5; i ++)
		{
			queue.dequeue();
		}
		assertEquals("El elemento retornado deberia ser Sara", "Sara", queue.dequeue());
	}

	public void testDequeue()
	{
		setUpEscenario2();
		assertEquals("El elemento retornado deber�a ser Dylan", "Dylan", queue.dequeue());
	}

	public void testIsEmptyQueue()
	{
		setUpEscenario1();
		assertEquals("La cola deber�a estar vac�a", true, queue.isEmptyQueue());

		setUpEscenario2();
		assertEquals("La cola no deber�a estar vac�a", false, queue.isEmptyQueue());
	}
	public void testSizeQueue()
	{
		setUpEscenario1( );
		queue.enqueue("Ana");
		assertEquals(1, queue.sizeQueue() );

		setUpEscenario2( );
		assertEquals(6, queue.sizeQueue());
	}
}
