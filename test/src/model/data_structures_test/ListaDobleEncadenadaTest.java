package model.data_structures_test;

import model.data_structures.ListaDobleEncadenada;
import junit.framework.TestCase;


public class ListaDobleEncadenadaTest extends TestCase
{
	private ListaDobleEncadenada<String> listaDobleEncadenada;

	private void setUpEscenario1( )
	{
		listaDobleEncadenada = new ListaDobleEncadenada<String>();
	}

	private void setUpEscenario2( )
	{
		listaDobleEncadenada = new ListaDobleEncadenada<String>();

		/*0*/ listaDobleEncadenada.agregarElementoFinall("Juan Pablo");
		/*1*/ listaDobleEncadenada.agregarElementoFinall("Sebasti�n");
		/*2*/ listaDobleEncadenada.agregarElementoFinall("Pedro");
		/*3*/ listaDobleEncadenada.agregarElementoFinall("Camila");
		/*4*/ listaDobleEncadenada.agregarElementoFinall("Sara");
	}

	public void testAgregarElementoFinal()
	{
		setUpEscenario1( );
		listaDobleEncadenada.agregarElementoFinall("Mar�a");
		assertEquals("El elemento deberia ser Mar�a", "Mar�a", listaDobleEncadenada.darElemento(0));
		
		setUpEscenario2( );
		listaDobleEncadenada.agregarElementoFinall("Andrea");
		assertEquals("El elemento deberia ser Andrea", "Andrea", listaDobleEncadenada.darElemento(5));
	}
	
	public void testDarElemento()
	{
		setUpEscenario2();
		assertEquals("El elemento deberia ser Sara", "Sara", listaDobleEncadenada.darElemento(4));
	}

	public void testDarNumeroDeElementos()
	{
		setUpEscenario1( );
		listaDobleEncadenada.agregarElementoFinall("Estructura");
		listaDobleEncadenada.darNumeroElementos();
		assertEquals(1 , listaDobleEncadenada.darNumeroElementos() );

		setUpEscenario2( );
		listaDobleEncadenada.darNumeroElementos();
		assertEquals(5 , listaDobleEncadenada.darNumeroElementos());
	}
	
	public void testDarElementoPosicionActual()
	{
		setUpEscenario2();
		listaDobleEncadenada.darElementoPosicionActual();
		assertEquals("Debe ser Juan Pablo", "Juan Pablo", listaDobleEncadenada.darElementoPosicionActual());
				
		setUpEscenario2();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();
		assertEquals("Deber�a ser Sara", "Sara", listaDobleEncadenada.darElementoPosicionActual());

	}
	
	public void testAvanzarSiguientePosicion()
	{
		setUpEscenario1();
		listaDobleEncadenada.avanzarSiguientePosicion();
		assertEquals("El nodo no deberia existir", false , listaDobleEncadenada.avanzarSiguientePosicion());
		
		setUpEscenario2();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();	
		assertEquals("El nodo no deberia existir", false , listaDobleEncadenada.avanzarSiguientePosicion());
		
		setUpEscenario2();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();
		assertEquals("El nodo deberia existir", true , listaDobleEncadenada.avanzarSiguientePosicion());
	}
	
	public void testRetrocederPosicionAnterior()
	{
		setUpEscenario2();
		listaDobleEncadenada.retrocederAnteriorPosicion();
		assertEquals("No se debe ejecutar porque hay un solo nodo", false , listaDobleEncadenada.retrocederAnteriorPosicion());
		
		setUpEscenario2();
		listaDobleEncadenada.avanzarSiguientePosicion();
		assertEquals("Debe retroceder a la posici�n anterior", true , listaDobleEncadenada.retrocederAnteriorPosicion());
	}
}
