package model.data_structures_test;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase
{
	private Stack<String> stack;

	private void setUpEscenario1( )
	{
		stack = new Stack<String>();
	}

	private void setUpEscenario2( )
	{
		stack = new Stack<String>();

		/*0*/ stack.push("Dylan");
		/*1*/ stack.push("Pablo");
		/*2*/ stack.push("Juan Manuel");
		/*3*/ stack.push("Melissa");
		/*4*/ stack.push("Laura");
		/*5*/ stack.push("Angie");
	}

	public void testPush()
	{
		setUpEscenario1( );
		stack.push("Carlos");
		assertEquals("El elemento retornado deberia ser Carlos", "Carlos", stack.pop());
		
		setUpEscenario2( );
		stack.push("Sara");
		assertEquals("El elemento retornado deberia ser Sara", "Sara", stack.pop());
	}
	
	public void testPop()
	{
		setUpEscenario2();
		for (int i = 0; i < 5; i++)
		{
			stack.pop();
		}
		assertEquals("El elemento retornado deber�a ser Dylan", "Dylan", stack.pop());
	}

	public void testIsEmptyStack()
	{
		setUpEscenario1();
		assertEquals("La pila deber�a estar vac�a", true, stack.isEmptyStack());
		
		setUpEscenario2();
		assertEquals("La pila no deber�a estar vac�a", false, stack.isEmptyStack());
	}
	public void testSizeStack()
	{
		setUpEscenario1( );
		stack.push("Ana");
		assertEquals(1, stack.sizeStack());

		setUpEscenario2( );
		assertEquals(6, stack.sizeStack());
	}
}
