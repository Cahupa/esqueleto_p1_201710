package view;

import java.io.IOException;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.ILista;
import model.vo.VOPelicula;
import model.logic.*;

public class VistaManejadorPeliculas {
	private SistemaRecomendacionPeliculas modelo;
	
	public static void main(String[] args) throws IOException {
		
		
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
			Controller.cargarPeliculas();
			Controller.cargarRatings();
			
			ILista<VOPelicula> lista=Controller.darPeliOrdenadas();
 
			for (VOPelicula voPelicula : lista) {
				System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
			}
					break;
				case 2:
					System.out.println("Ingrese subcadena de b�squeda:");
					String busqueda=sc.next();
					ILista<VOPelicula> lista1=Controller.darListaPeliculas(busqueda);
					System.out.println("Se encontraron "+lista1.darNumeroElementos()+" elementos");
					for (VOPelicula voPelicula : lista1) {
						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
					}
					break;
				case 3:
					System.out.println("Ingrese el a�o de b�squeda");
					int agno=sc.nextInt();
					ILista<VOPelicula> listaPeliculasAgno=Controller.darPeliculasPorAgno(agno);
					System.out.println("Se encontraron "+listaPeliculasAgno.darNumeroElementos()+" elementos");
					for (VOPelicula voPelicula : listaPeliculasAgno) {
						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
					}
					break;
				
				case 6:	
					fin=true;
					break;
			}
			
			
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cree una nueva colecci�n de pel�culas (data/movies.csv)");
		System.out.println("2. Buscar pel�culas por subcadena");
		System.out.println("3. Buscar pel�culas por a�o");
		System.out.println("4. Dar pel�culas a�o siguiente");
		System.out.println("5. Dar pel�culas a�o anterior");
		System.out.println("6. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
	}

}
