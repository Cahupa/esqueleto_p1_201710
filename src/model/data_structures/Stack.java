package model.data_structures;

public class Stack<T> implements IStack<T>
{
	private NodoDoble<T> tope = null;
	private int size = 0;
	
	
	public void push(T item) 
	{
		NodoDoble<T> newNodo = new NodoDoble<T>(item);
		newNodo.setItem(item);

		if(size == 0)
		{
			tope = newNodo;
			size++;
		}
		else
		{
			newNodo.setSiguiente(tope);
			tope = newNodo;
			size++;
		}
	}

	
	public T pop()
	{
		if(tope == null)
		{
			return null;
		}
		else
		{
			NodoDoble<T> resp = tope;
			tope = tope.darSiguiente();
			size--;
			return resp.getItem();
		}
	}

	
	public boolean isEmptyStack() 
	{
		return tope == null;
	}


	public int sizeStack() 
	{
		return size;
	}


	public void push(String line) {
		T item = null;
		// TODO Auto-generated method stub
		NodoDoble<T> newNodo = new NodoDoble<T>(item);
		newNodo.setItem(item);

		if(size == 0)
		{
			tope = newNodo;
			size++;
		}
		else
		{
			newNodo.setSiguiente(tope);
			tope = newNodo;
			size++;
		}
	}


	public void push1(Double recomendacion) {
		// TODO Auto-generated method stub
		T item = null;
		// TODO Auto-generated method stub
		NodoDoble<T> newNodo = new NodoDoble<T>(item);
		newNodo.setItem(item);

		if(size == 0)
		{
			tope = newNodo;
			size++;
		}
		else
		{
			newNodo.setSiguiente(tope);
			tope = newNodo;
			size++;
		}
	}


}
