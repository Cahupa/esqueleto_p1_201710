package model.data_structures;

import model.vo.VOGeneroPelicula;

public interface ILista<T> extends Iterable<T> 
{
	public void agregarElementoFinal(T elem);
	
	public T darElemento(int pos) throws NoExisteElemento;
	
	public T eliminarElemento(int pos);
	
	public int darNumeroElementos();

	void agregarElementoFinall(T elem);

	void agregarElementoFinal(String string);

	void agregarElementoFinalVo(VOGeneroPelicula voGeneroPelicula);
}
