package model.data_structures;

public interface IQueue<T>
{	
	/**
	 * A�ade el elemento elem al final de la lista
	 * @param elem
	 */
	public void enqueue (T item);
	
	/**
	 * Elimina el elemento de la primera posici�n de la cola
	 * @return Retorna el elemento eliminado
	 * @throws NoExisteElemento 
	 */
	public T dequeue() throws NoExisteElemento;
	
	/**
	 * Indica si la cola est� vac�a
	 * @return true si est� vacio, false si la cola contiene elementos
	 */
	public boolean isEmptyQueue();
	
	/**
	 * N�mero de elementos en la cola
	 * @return Retorna el tamanio de la cola
	 */
	public int sizeQueue();
}
