package model.data_structures;

public class Queue<T> implements IQueue<T>
{
	private NodoDoble<T> first = null;
	private NodoDoble<T> last = null;
	private int size = 0;

	
	
	public void enqueue(T item) 
	{
		NodoDoble<T> newNodo = new NodoDoble<T>(item);

		if(size == 0)
		{
			first = newNodo;
			last = newNodo;
		}
		else
		{
			last.setSiguiente(newNodo);
			last = newNodo;
		}
		size++;
	}

	
	public T dequeue()
	{
		if(first == null)
		{
			return null;
		}
		else
		{
			NodoDoble<T> resp = first;
			first = first.darSiguiente();
			size--;
			return resp.getItem();
		}
	}

	
	public boolean isEmptyQueue() 
	{
		return first == null && last == null;
	}

	
	public int sizeQueue() 
	{
		return size;
	}
}