package model.data_structures;

import java.util.Iterator;

import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class ListaDobleEncadenada<T> implements ILista<T>
{
	private NodoDoble<T> cabeza = null;
	private NodoDoble<T> ultimo = null;
	private int nodoActual = 0;
	private int size = 0;
	private int tama�o=0;


	public Iterator<T> iterator() 
	{
		return new Iterator<T>()
		{
			NodoDoble<T> actual = null;

			public boolean hasNext() 
			{
				if(size == 0)
					return false;

				if(actual == null)
					return true;

				return actual.darSiguiente() != null;
			}

	
			public T next() 
			{

				if(actual == null)
				{
					actual = cabeza;
					return actual.getItem();
				}
				if (actual.darSiguiente() == null) 
				{
					try 
					{
						throw new NoExisteElemento("Siguiente de :"+actual.toString());
					}
					catch (NoExisteElemento e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					actual = actual.darSiguiente();
				}
				return actual.getItem();
			}
		};
	}


	public void agregarElementoFinal(T elem,int siz) 
	{
		NodoDoble<T> nuevoNodo = new NodoDoble<T>(elem);

		
		if (cabeza == null)
		{
			cabeza = nuevoNodo;
			ultimo = nuevoNodo;
		}
		else
		{
			NodoDoble<T> actual = cabeza;
			while (actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
			actual.setSiguiente(nuevoNodo);
			ultimo = nuevoNodo;
		}
		tama�o=siz;
		size++;
	}


	public T darElemento(int pos) throws ArrayIndexOutOfBoundsException
	{
		T elem = null;
		if(pos >= size)
		{
			throw new ArrayIndexOutOfBoundsException("No existe la posici�n ingresada");
		}
		else
		{
			int cont = 0;
			NodoDoble<T> actual = cabeza;
			while(actual != null)
			{
				if(cont == pos)
				{
					elem = actual.getItem();
				}
				actual = actual.darSiguiente();
				cont++;
			}
		}
		return elem;
	}



	public int darNumeroElementos() 
	{
		return size;
	}
	public void setSize(int n)
	{
		size= n;
	}

	public T darElementoPosicionActual() 
	{
		return darElemento(nodoActual);
	}

	public boolean avanzarSiguientePosicion() 
	{
		boolean resultado = false;
		if(nodoActual + 1 >= size)
			resultado = false;
		else
		{
			nodoActual++;
			resultado = true;
		}

		return resultado;
	}

	public boolean retrocederAnteriorPosicion() 
	{
		boolean resultado = false;
		if(nodoActual - 1 >= 0 && nodoActual - 1 <= size)
		{
			resultado = true;
			nodoActual--;
		}
		return resultado;
	}

	public NodoDoble<T> darPrimerElemento()
	{
		return cabeza;
	}
	public NodoDoble<T> darUltimoElemento()
	{
		return ultimo;
	}


	public T eliminarElemento(int pos) 
	{
		return null;
	}


	@Override
	public void agregarElementoFinall(T elem) {
		// TODO Auto-generated method stub
NodoDoble<T> nuevoNodo = new NodoDoble<T>(elem);

		
		if (cabeza == null)
		{
			cabeza = nuevoNodo;
			ultimo = nuevoNodo;
		}
		else
		{
			NodoDoble<T> actual = cabeza;
			while (actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
			if(actual.darSiguiente()==null)
			{
				actual.setSiguiente(nuevoNodo);
				ultimo = nuevoNodo;
				
			}
		}
		
	}


	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void agregarElementoFinal(String string) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void agregarElementoFinalVo(VOGeneroPelicula voGeneroPelicula) {
		// TODO Auto-generated method stub
		
	}





}