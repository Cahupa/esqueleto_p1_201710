package model.data_structures;

public class NoExisteElemento extends Exception {
	
	public NoExisteElemento(String mensaje) {
        super("No existe el Elemento:"+mensaje);
    }

}
