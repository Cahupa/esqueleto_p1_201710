package model.vo;

public class VOTag {
	private String tag;
	private long timestamp;
	private double moId;
	private double uId;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public void setMovieID(double mid)
	{
		moId=mid;
	}
	public void setUserID(double usId)
	{
		uId=usId;
	}
	public double getUserId()
	{
		return uId;
	}
	public double getMovieID()
	{
		return moId;
	}
	
}
