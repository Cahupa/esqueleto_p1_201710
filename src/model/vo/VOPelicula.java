package model.vo;

import java.util.ArrayList;
import java.util.Arrays;

import model.data_structures.ILista;
import model.data_structures.Stack;

public class VOPelicula implements Comparable<VOPelicula>{
	
	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;


	private ILista<String> tagsAsociados;
	private ILista<String> generosAsociados;
	
	
	/*public VOPelicula()
	{
		titulo=titul;
		agnoPublicacion=a�o;
		ILista<String> lista= (ILista<String>) Arrays.asList(generos);
	}
	*/
	public long getIdUsuario() {
		return idPelicula;
	}
	public void setIdUsuario(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	public void setId(long id)
	{
		idPelicula=id;
		//System.out.println(idPelicula);
		//System.out.println(titulo);
	}
	public long getId()
	{
		return idPelicula;
	}
	
	@Override
	public int compareTo(VOPelicula goku) {
		int compara;;
		
		if(this.getAgnoPublicacion()>goku.getAgnoPublicacion())
		{
			compara=-1;
		}
		else if(this.getAgnoPublicacion()<goku.getAgnoPublicacion())
		{
			compara=1;
		}
		else
		{
			if(this.getPromedioRatings()>goku.getPromedioRatings())
			{
				compara=-1;
			}
			else if(this.getPromedioRatings()<goku.getPromedioRatings()){
				compara=1;
			}
			else{
				compara=this.getTitulo().compareTo(goku.getTitulo());
				
			}
			
			
		}
		return compara;
	}
	public void setRatingAsinadoRecomendacion(Double rating) {
		// TODO Auto-generated method stub
		
	}
	

}
