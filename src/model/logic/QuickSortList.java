package model.logic;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoSencillo;
import model.vo.VOPelicula;

public class QuickSortList {

	public static final String  FRATING="Fecha de creacion del rating";
	public static final String  PRATING="Promedio rating";
	public static final String  NOMBRE="Fecha de creacion del rating";
	public static final String  FECHA="Fecha de creacion del rating";
	public static final String  NRATING="Fecha de creacion del rating";
	public static final String  APT="Fecha de creacion del rating";
	public static final String DIFERAT = null;
	private static String ordenar; 
	public static <T extends Comparable<T>>void sort (ListaEncadenada<T> lista,String expresion)
	{
		ordenar=expresion;
		quicksort(lista,0,lista.darNumeroElementos()-1);
	}
	private static <T extends Comparable<T>> void quicksort(ListaEncadenada<T> lista,int low,int high)

	{
		if(high<=low)return;
		int pivot= partition(lista,low,high);
		quicksort(lista,low,(pivot-1));
		quicksort(lista,pivot+1,high);
	}
	private static <T extends Comparable<T>> int partition(ListaEncadenada<T> lista,int low,int high)
	{

		int i = low+1;
		int j = high;
		while(i <= j)
		{
			if( compare(lista.darElemento(i), lista.darElemento(low)) <= 0)
			{
				i++;
			}
			else if( compare(lista.darElemento(j), lista.darElemento(low)) > 0)
			{
				j--;
			}
			else if( j < 1)
			{
				break;
			}
			else
			{
				exchange(lista,i,j);
			}
		}
		exchange(lista, low, j);
		return j;
	}		






	private static <T extends Comparable<T>> int compare(T primero, T segundo)
	{
		if(ordenar.equals(NOMBRE)||ordenar.equals(FRATING))
		{
			return ((VOPelicula) primero).compareTo((VOPelicula) segundo);
		}
		else if(ordenar.equals(PRATING)||ordenar.equals(FECHA)||ordenar.equals(NRATING))
		{
			VOPelicula unio = (VOPelicula) primero;
			VOPelicula dos =(VOPelicula) segundo;
			Double d1=0.0;
			Double d2=0.0;
			if (ordenar.equals(PRATING))
			{
				d1= unio.getPromedioRatings();
				d2=  dos.getPromedioRatings();

			}
			else if(ordenar.equals(FECHA))
			{
				d2= (double) unio.getAgnoPublicacion();
				d1= (double) dos.getAgnoPublicacion();
			}
			else if(ordenar.equals(NRATING))
			{
				d1= (double) unio.getNumeroRatings();
				d2= (double) dos.getNumeroRatings();

			}
			if(d1>d2)
			{
				return -1;
			}
			else
			{
				return 1;
			}

		}
		else
		{
			return 0;
		}
	}
	private static <T extends Comparable<T> >void exchange(ListaEncadenada<T> lista,int i,int j)
	{
		T t1=lista.darElemento(i);
		T t2=lista.darElemento(j);
	}
	public static boolean estaOrdenada(ListaEncadenada<VOPelicula> pelisVOPP,
			String diferat2) {
		// TODO Auto-generated method stub
		return false;
	}
}
