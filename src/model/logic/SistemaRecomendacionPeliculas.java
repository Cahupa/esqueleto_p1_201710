package model.logic;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.NoExisteElemento;
import model.data_structures.NodoDoble;
import model.data_structures.NodoSencillo;
import model.data_structures.Stack;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas
{
	ListaDobleEncadenada<VOPelicula> listaPel= null;
	ListaDobleEncadenada<VOPelicula> listaPeliculasO= null;
	ListaDobleEncadenada<VORating> listaRatings=null;
	ListaDobleEncadenada<VOUsuarioGenero> listaUsuariosGenero=null;
	ListaDobleEncadenada<VOTag> listaTags=null;
	ListaDobleEncadenada<VOPelicula> listaPeliculas=null;
	ListaDobleEncadenada<String> listaGenero=null;
	ListaDobleEncadenada<VOGeneroPelicula> listaGP=null;
	ListaDobleEncadenada<VOUsuarioConteo> listaConteo= null;
	ListaEncadenada<VOGeneroUsuario> listaGenUsu= null;
	String rutaPeliculas = "data/movies.csv";
	String rutaRatings = "data/ratings.csv";
	String rutaTags = "data/tags.csv";
	Stack<VOOperacion> lista=null;
	Stack<Double> misRecomendaciones;
	Stack<Double> promedioR= null;
	int cantidadPelis=0;

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		//Version 2.0 RIVERPLATE CAMPEON DE AMERICA	
		BufferedReader br = null;
		listaPel = new ListaDobleEncadenada<>();

		try
		{
			br = new BufferedReader(new FileReader("data/movies.csv"));
			String line = br.readLine();
			line = br.readLine();
			while(line != null)
			{
				ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>();
				String nombreP = "";
				int anioP = 0;
				String ge = line.substring(line.lastIndexOf(",")+1);
				String anNom = line.substring(0, line.lastIndexOf(","));
				String id= anNom.split(",")[0];
				int idInt= Integer.parseInt(id);

				if(ge.contains("\""))
				{
					ge = ge.replaceAll("\"", "");
				}

				String generos[] = ge.split("\\|");

				for(int i = 0; i < generos.length; i++ )
				{
					listaGeneros.agregarElementoFinall(generos[i]);
				}

				String anio = "";
				if (anNom.contains("("))
				{
					anio = anNom.substring(anNom.lastIndexOf("(")+1, anNom.lastIndexOf(")"));
					anNom = anNom.substring(0, anNom.lastIndexOf("("));
				}

				if(anNom.contains("\""))
				{
					nombreP = anNom.replaceAll("\"", "");
				}
				else 
				{
					nombreP = anNom;
				}

				if(!anio.isEmpty())
				{
					anioP = Integer.parseInt(anio.substring(0, 4));
				}
				agregarPelicula(nombreP, anioP, generos);
				cantidadPelis++;
				listaPel.setSize(cantidadPelis);
				line = br.readLine();
			}	
			br.close();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}


	@Override
	public boolean cargarRatingsSR(String rutaRatings) {

		BufferedReader br = null;
		listaRatings = new ListaDobleEncadenada<>();
		try
		{
			br = new BufferedReader(new FileReader(rutaRatings));
			String line = br.readLine();
			line = br.readLine();
			while(line != null)
			{
				String[] dat= line.split(",");
				long userId=Long.parseLong(dat[0]);

				long movieId=Long.parseLong(dat[1]);
				double  ratingP=Double.parseDouble(dat[2]);
				long timesStamp=Long.parseLong(dat[3]);

				rating(movieId,ratingP);
				agregarRating(userId, movieId, ratingP);
				line = br.readLine();
			}
			br.close();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	@Override
	public boolean cargarRecomendaciones( String rutaRecomendacion)
	{
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(rutaRecomendacion));
			String line = br.readLine();
			line = br.readLine();
			while(line != null)
			{
				misRecomendaciones.push(line);
				line = br.readLine();
			}
			br.close();
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	public boolean cargarTagsSR(String rutaTags) {
		BufferedReader br = null;
		listaRatings = new ListaDobleEncadenada<>();

		try
		{
			br = new BufferedReader(new FileReader(rutaTags));
			String line = br.readLine();
			line = br.readLine();

			while(line != null)
			{
				String[] dat= line.split(",");
				Double userId=Double.parseDouble(dat[0]);
				Double movieId=Double.parseDouble(dat[1]);
				String tag = dat[2];
				long timesStamp=Long.parseLong(dat[3]);

				line = br.readLine();
			}	
			br.close();


			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}


	@Override
	public int sizeMoviesSR() {
		return cantidadPelis;
	}

	@Override
	public int sizeUsersSR() {
		return listaUsuariosGenero.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		return listaTags.darNumeroElementos();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR (int n) throws NoExisteElemento
	{
		cargarPeliculasSR(rutaPeliculas);
		cargarRatingsSR(rutaRatings);
		ILista<VOGeneroPelicula> temp = null;
		ILista<VOGeneroPelicula> nueva = null;

		for(int i = 0; i < listaPel.darNumeroElementos(); i++)
		{
			VOPelicula pelicula = listaPel.darElemento(i);
			VORating rating = listaRatings.darElemento(i);
			pelicula.setRatingAsinadoRecomendacion(rating.getRating());
		}
		for (int j = 0; j < listaGenero.darNumeroElementos(); j++)
		{
			String generoAct = listaGenero.darElemento(j);
			for(int k = 0; k < listaPel.darNumeroElementos(); k++)
			{
				VOPelicula actualPeli = listaPel.darElemento(k);
				ILista<String> genero = actualPeli.getGenerosAsociados();
				for(int l = 0; l < genero.darNumeroElementos(); l++)
				{
					String variable = genero.darElemento(l);
					if(variable.equals(generoAct))
					{
						nueva.agregarElementoFinal(variable);
					}
				}
			}

		}
		return temp;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		ILista<VOPelicula> catalogo= new ListaDobleEncadenada<>();

		sortVoPelis();

		catalogo=listaPeliculasO;


		return catalogo;
	}
	public void sortVoPelis()
	{

	}

	public ListaDobleEncadenada<VOGeneroPelicula> peliculaGenero()
	{
		ListaDobleEncadenada<VOGeneroPelicula> linked=new ListaDobleEncadenada<>();
		return linked;

	}
	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() throws NoExisteElemento 
	{
		ILista<VOGeneroPelicula> nuevaLista = new ListaDobleEncadenada<>();
		cargarPeliculasSR(rutaPeliculas);
		cargarRatingsSR(rutaRatings);

		Comparator<VOPelicula> comp = new Comparator<VOPelicula>() 
		{
			@Override
			public int compare(VOPelicula pel1, VOPelicula pel2)
			{
				int temp = 0;
				if(pel1.getPromedioRatings() < pel2.getPromedioRatings()){
					temp = -1;
				}
				else if(pel1.getPromedioRatings() > pel2.getPromedioRatings()){
					temp = 1;
				}
				else if(pel1.getPromedioRatings() == pel2.getPromedioRatings()){
					temp = 0;
				}
				return temp;
			}
		};
		ILista<VOGeneroPelicula> lista = new ListaEncadenada<>();
		for(int i = 0; i < listaGP.darNumeroElementos(); i++){
			QuickSortList.sort((ListaEncadenada<VOPelicula>) lista.darElemento(i).getPeliculas(), QuickSortList.DIFERAT);
		}
		for(int i = 0; i < listaGP.darNumeroElementos(); i++){
			nuevaLista.agregarElementoFinal(lista.darElemento(i));
		}
		return nuevaLista;
	}

	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		if(listaGP.darNumeroElementos() == 0);
		for (int i = 0; i < listaGP.darNumeroElementos(); i++) {
			VOGeneroPelicula voG = listaGP.darElemento(i);
			ILista<VOPelicula> voGP = voG.getPeliculas();
			QuickSortList.sort((ListaEncadenada<VOPelicula>) voGP, QuickSortList.FECHA);
		}
		return listaGP;
	}


	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) throws NoExisteElemento {

		if(sizeRecomendaciones() == 0)cargarRecomendaciones(rutaRecomendacion);
		ILista<VOPeliculaPelicula> voPPL = new ListaEncadenada<>();
		Double recomendacion;
		String gene;
		Double rating = null;
		VOPelicula voP;
		VOPeliculaPelicula voPP;
		Long idPelicula;
		Stack<String> recomendaciones = new Stack<>();
		int max = sizeRecomendaciones();

		for (int i = 0; i < n && i < max; i++) 
		{
			recomendacion = misRecomendaciones.pop();

			voP = listaPel.darPrimerElemento().getItem();
			recomendaciones.push1(recomendacion);
			if (voP != null) 
			{
				voP.setRatingAsinadoRecomendacion(rating);
				voPP = new VOPeliculaPelicula();
				voPP.setPelicula(voP);
				ILista<VOPelicula> pelisVOPP = new ListaEncadenada<>();
				gene = voP.getGenerosAsociados().darElemento(0);

				for (int j = 0; j < listaGenero.darNumeroElementos(); j++) 
				{
					VOGeneroPelicula voG = listaGP.darElemento(j);
					if (voG.getGenero().equalsIgnoreCase(gene)) 
					{
						ILista<VOPelicula> listGP = voG.getPeliculas();
						Double ratiP = 0.0;
						Double diferencia = 0.0;

						for (int k = 0; k < listGP.darNumeroElementos(); k++) 
						{
							VOPelicula peli = listGP.darElemento(k);

							ratiP = peli.getPromedioRatings();

							diferencia = (rating-ratiP);
							diferencia = (diferencia < 0 ? -diferencia : diferencia);

							if ( diferencia <= 0.5) 
							{

								pelisVOPP.agregarElementoFinal(peli);
							}
						}
						QuickSortList.sort((ListaEncadenada<VOPelicula>) pelisVOPP, QuickSortList.DIFERAT);
						if(QuickSortList.estaOrdenada((ListaEncadenada<VOPelicula>) pelisVOPP, QuickSortList.DIFERAT))voPP.setPeliculasRelacionadas(pelisVOPP);
					}
				}
				voPPL.agregarElementoFinal(voPP);
			}
		}
		while(!recomendaciones.isEmptyStack())misRecomendaciones.push(recomendaciones.pop());
		return voPPL;
	}

	private int sizeRecomendaciones() {
		// TODO Auto-generated method stub
		return misRecomendaciones.sizeStack();
	}


	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		return null;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) throws NoExisteElemento 
	{	
		ListaDobleEncadenada<VORating> lr= listaRatings;
		NodoDoble<VORating> nodo= lr.darPrimerElemento();
		ListaDobleEncadenada<VOGeneroPelicula> gp=listaGP;
		NodoDoble<VOGeneroPelicula>nodogp= gp.darPrimerElemento();
		ListaDobleEncadenada<VOUsuarioConteo> uc=listaConteo;
		NodoDoble<VOUsuarioConteo>nuc= uc.darPrimerElemento();


		while(nodo.darSiguiente()!=null)
		{
			if(uc.darNumeroElementos()!=0)
			{
				while(nuc.darSiguiente()!=null)
				{
					if(nodo.getItem().getIdUsuario()==nuc.getItem().getIdUsuario())
					{
						nuc.getItem().setConteo(nuc.getItem().getConteo()+1);
						generoUsuario(nuc.getItem().getIdUsuario(), uc);
					}
					else
					{
						nuc= nuc.darSiguiente();
					}
				}
			}
			else if(uc.darNumeroElementos()==0||uc==null)
			{
				VOUsuarioConteo nuevo= new VOUsuarioConteo();
				nuevo.setConteo(1);
				nuevo.setIdUsuario(nodo.getItem().getIdUsuario());
				uc.agregarElementoFinall(nuevo);
				generoUsuario(nuevo.getIdUsuario(), uc);
			}
			nodo= nodo.darSiguiente();
		}
		ILista<VOGeneroUsuario> nose=listaGenUsu;
		return nose;
		//VO USUARIO CONTEO HECHO HAY UNA LISTA DE CONTEO POR USUARIO
	}


	public void  generoUsuario(long userID, ListaDobleEncadenada<VOUsuarioConteo>ucon) throws NoExisteElemento
	{
		ListaEncadenada<VOGeneroUsuario> gus= listaGenUsu;
		for(int i=0;i<gus.darNumeroElementos();i++)
		{
			VOGeneroUsuario nGus= gus.darElemento(0);
			NodoDoble<VOPelicula> nodo1=listaPel.darPrimerElemento();
			while(nodo1.darSiguiente()!=null)
			{
				if(nGus==null)
				{
					VOGeneroUsuario nuevo= new VOGeneroUsuario();
					if(nodo1.getItem().getIdUsuario()==userID)
					{
						for(int k=0;k<nodo1.getItem().getGenerosAsociados().darNumeroElementos();k++)
						{
							VOGeneroUsuario u= new VOGeneroUsuario();
							u.setGenero(nodo1.getItem().getGenerosAsociados().darElemento(k));
							u.setUsuarios(ucon);
							gus.agregarElementoFinall(u);
							listaGenUsu=gus;

						}
					}
				}
				else
				{
					for(int j=0;j<gus.darNumeroElementos();j++)
					{
						gus.darElemento(j).setUsuarios(ucon);

					}
				}
			}
		}
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() 
	{
		return null;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		return null;
	}

	@Override
	public <T> ILista<VOUsuarioGenero> opinionTagsGeneroSR() 
	{
		ILista<VOUsuarioGenero> nuevaLista = new ListaEncadenada<>();
		
		cargarPeliculasSR(rutaPeliculas);
		cargarRatingsSR(rutaRatings);
		
		Comparator<VOPelicula> comparador = new Comparator<VOPelicula>() {
			@Override
			public int compare(VOPelicula pel1, VOPelicula pel2){
				int temp = 0;
				if(pel1.getAgnoPublicacion() < pel2.getAgnoPublicacion()){
					temp = -1;
				}
				else if(pel1.getAgnoPublicacion() > pel2.getAgnoPublicacion()){
					temp = 1;
				}
				else if(pel1.getAgnoPublicacion() == pel2.getAgnoPublicacion()){
					temp = 0;
				}
				return temp;
			}
		};
		ILista<VOGeneroPelicula> lista = new ListaEncadenada<>();
		for(int i = 0; i < listaGP.darNumeroElementos(); i++){
			QuickSortList.sort((ListaEncadenada<VOPelicula>) lista.darElemento(i).getPeliculas(), QuickSortList.DIFERAT);
		}
		for(int i = 0; i < listaGP.darNumeroElementos(); i++){
			nuevaLista.agregarElementoFinalVo(lista.darElemento(i));
		}
		return nuevaLista;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) 
	{
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) 
	{
		cargarTagsSR(rutaTags);
		double parametro = (double) idPelicula;
		ILista<VOTag> temp = null;

		for(int i = 0; i < listaTags.darNumeroElementos(); i++)
		{
			VOTag tag = listaTags.darElemento(i);
			if(parametro == tag.getUserId())
			{
				temp.agregarElementoFinal(tag);
			}
		}
		return temp;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {

		VOOperacion nuevaOp= new VOOperacion();
		nuevaOp.setOperacion(nomOperacion);
		nuevaOp.setTimestampFin(tfin);
		nuevaOp.setTimestampInicio(tinicio);

	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		lista= null;
		lista= new Stack<VOOperacion>();

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		ILista<VOOperacion> lastOp= (ILista<VOOperacion>) new Stack();
		int nm=n;
		while(nm!=0){
			VOOperacion op= lista.pop();
			lastOp.agregarElementoFinal(op);
			lista.push(op);
		}
		nm--;
		return lastOp;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		int nm=n;
		while(nm!=0)
		{
			lista.pop();
		}
		nm--;

	}

	public void guardarHistorial(VOOperacion operacion)
	{
		lista=new Stack<>();
		lista.push(operacion);

	}


	@Override
	public void agregarRating(long idUsuario, long idPelicula, double rating) {

		VORating nuevoRating= new VORating();
		nuevoRating.setIdPelicula(idPelicula);
		nuevoRating.setIdUsuario(idUsuario);
		nuevoRating.setRating(rating);
		listaRatings.agregarElementoFinall(nuevoRating);
	}

	public void generPel() throws NoExisteElemento
	{
		ListaDobleEncadenada<VOGeneroPelicula> nueva= new ListaDobleEncadenada<>();
		ListaDobleEncadenada<VOPelicula> lp= listaPel;
		NodoDoble<VOPelicula> nodo=lp.darPrimerElemento();
		NodoDoble<VOGeneroPelicula> nodoG=listaGP.darPrimerElemento();
		while(nodo.darSiguiente()!=null)
		{
			if(nodoG==null )
			{
				VOPelicula nk= nodo.getItem();
				for(int k=0;k<nodo.getItem().getGenerosAsociados().darNumeroElementos();k++)
				{
					VOGeneroPelicula nuevo= new VOGeneroPelicula();
					nuevo.setGenero(nodo.getItem().getGenerosAsociados().darElemento(k));
					ListaEncadenada<VOPelicula> ilisEncadenada= new ListaEncadenada<>();
					ilisEncadenada.agregarElementoFinall(nodo.getItem());
					nuevo.setPeliculas(ilisEncadenada);
					nueva.agregarElementoFinall(nuevo);
				}
			}
			else //if(nodo.getItem().getGenerosAsociados().darNumeroElementos()>0)
			{

				for(int i=0;i<nodo.getItem().getGenerosAsociados().darNumeroElementos();i++)
				{
					String genero= nodo.getItem().getGenerosAsociados().darElemento(i);
					if(genero.equalsIgnoreCase(nodoG.getItem().getGenero()))
					{
						VOPelicula n=nodo.getItem();
						ILista<VOPelicula>e=nodoG.getItem().getPeliculas();
						e.agregarElementoFinall(n);
						nodoG.getItem().setPeliculas(e);
					}
				}
			}
		}
	}
	public void agregarTag(double userId, double movieId,String tag,Long ts)
	{
		VOTag nuevoTag= new VOTag();
		nuevoTag.setTag(tag);
		nuevoTag.setTimestamp(ts);
		nuevoTag.setMovieID(movieId);
		nuevoTag.setUserID(userId);	
		listaTags.agregarElementoFinall(nuevoTag);
	}

	public void rating(long id,double rating )
	{
		boolean c= false;
		NodoDoble<VOPelicula> actual= listaPel.darPrimerElemento();

		//while(nodo.getItem()!=null&&nodo.darSiguiente()!=null)
		while(actual.darSiguiente()!=null||c==false)
		{
			VOPelicula pel= actual.getItem();
			if(pel.getId()==id)
			{
				if(pel.getNumeroRatings()==0)
				{
					pel.setPromedioRatings(rating);
					c=true;
				}
				else
				{
					int nR= pel.getNumeroRatings();
					double pR=pel.getPromedioRatings();
					double nose= (nR*pR)+rating;
					double noseX2= nose/(pR+1);
					pel.setPromedioRatings(noseX2);
					pel.setNumeroRatings(nR+1);
					c=true;

				}


			}
			actual= actual.darSiguiente();
		}



	}
	public  void agregarTagsAPeliculas()
	{
		int i=0;
		ListaDobleEncadenada<String> tempTags= new ListaDobleEncadenada<>();
		NodoDoble<VOTag> nActual=listaTags.darPrimerElemento();
		NodoDoble<VOPelicula> nPelA=listaPel.darPrimerElemento();
		while(nPelA.darSiguiente()!=null)
		{
			while(nActual.darSiguiente()!=null)
			{
				double movie= nActual.getItem().getMovieID();
				double movieID=nPelA.getItem().getId();
				if(movie==movieID)
				{
					i++;
					nPelA.getItem().setNumeroTags(i);
					tempTags.agregarElementoFinall(nActual.getItem().getTag());

				}

				else{
					nActual=nActual.darSiguiente();
				}
			}
			ILista<String> nueva= tempTags;
			nPelA.getItem().setTagsAsociados(nueva);
			nPelA= nPelA.darSiguiente();

		}

	}
	public ILista<VOPelicula> peliculasPromedioAlto()
	{
		return listaPel;

	}

	public long agregarPelicula(String titulo, int agno, String[] generos) {


		VOPelicula peli = new VOPelicula();

		ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>();
		for(int i = 0; i < generos.length; i++ )
		{
			listaGeneros.agregarElementoFinall(generos[i]);
		}

		int coma = titulo.indexOf(",");
		Long id = Long.parseLong(titulo.substring(0, coma));
		peli.setTitulo(titulo.substring(coma+1));
		peli.setAgnoPublicacion(agno);
		peli.setGenerosAsociados(listaGeneros);
		peli.setId(id);

		listaPel.agregarElementoFinall(peli);
		return id;

	}
	public void testerPel()
	{

		VOPelicula imprimir;
		NodoDoble<VOPelicula> nodo=listaPel.darPrimerElemento();
		while(nodo.getItem()!=null&&nodo.darSiguiente()!=null)
		{
			imprimir= nodo.getItem();

			//System.out.println("TITULO PELICULA:"+imprimir.getAgnoPublicacion());

			nodo= nodo.darSiguiente();

		}

	}

	public void testerRatings(){

		VORating imprimir; 
		NodoDoble<VORating> nodo= listaRatings.darPrimerElemento();
		//while(nodo.getItem()!=null&&nodo.darSiguiente()!=null)

		//{
		//imprimir=nodo.getItem();
		//System.out.println("RATINGS:"+imprimir.getIdPelicula());
		//nodo= nodo.darSiguiente();
		//}
	}








}
