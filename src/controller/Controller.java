package controller;

import model.data_structures.ILista;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.VOPelicula;
import java.io.IOException;
import api.ISistemaRecomendacionPeliculas;

public class Controller 
{
	private static SistemaRecomendacionPeliculas sistema = new SistemaRecomendacionPeliculas();

	public static void cargarPeliculas() throws IOException 
	{

		sistema.cargarPeliculasSR("data/movies.csv");
		System.out.println("carga peliculas");
		sistema.cargarRatingsSR("data/ratings.csv");
		System.out.println("carga ratings");
		sistema.testerPel();
	}


	public static ILista<VOPelicula> darListaPeliculas(String busqueda) 
	{
		// TODO Auto-generated method stub
		return null;
	}


	public static ILista<VOPelicula> darPeliculasPorAgno(int agno) 
	{
		ILista<VOPelicula>c=sistema.catalogoPeliculasOrdenadoSR();
		return c;
	}
	public static ILista<VOPelicula >darPeliOrdenadas()
	{
	ILista<VOPelicula>c=sistema.catalogoPeliculasOrdenadoSR();
	return c;
	}


	public static void cargarRatings() {
		// TODO Auto-generated method stub
		sistema.cargarRatingsSR("data/ratings.csv");
		
	}


	public static void darTama�o() {
	System.out.println(sistema.sizeMoviesSR()); 
		
	}


}

